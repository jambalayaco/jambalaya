export function indexOfObjectWithKeyValue (arr, key, value) {
  let index = -1
  arr.forEach((element, i) => {
    if (element[key] === value) {
      index = i
    }
  })
  return index
}
