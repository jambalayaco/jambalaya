import GitLab from './gitlab'
import GitHub from './github'
import AbstractCMS from './abstract'

export default class CMS {
  services = {
    GITLAB: 'gitlab',
    GITHUB: 'github'
  }

  constructor (service, id) {
    switch (service) {
      case this.services.GITLAB:
        return new GitLab(id)
      case this.services.GITHUB:
        return new GitHub(id)
      default:
        return new AbstractCMS(id)
    }
  }
}
