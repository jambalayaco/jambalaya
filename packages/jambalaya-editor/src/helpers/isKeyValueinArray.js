export function isKeyValueInArray (arr, key, value) {
  return arr.some(function (element) {
    return element[key] === value
  })
}
