import Vue from 'vue'

const state = {
  changesList: {},
  saveStatus: 'no changes'
}

const getters = {
  changesListCount: state => Object.keys(state.changesList).length,
  changesListCheckedCount: (state) => {
    let count = 0
    Object.keys(state.changesList).forEach((key) => {
      if (state.changesList[key].checked) {
        count += 1
      }
    })
    return count
  },
  recentChanges: state => {
    let recent = Object.entries(state.changesList).map(item => {
      return { path: item[0], value: item[1] }
    })
    recent.sort((a, b) => {
      if (a.value.timestamp < b.value.timestamp) {
        return 1
      }
      if (a.value.timestamp > b.value.timestamp) {
        return -1
      }
      return 0
    })
    return recent.slice(0, 5)
  }
}

const mutations = {
  updateChangesList (state, payload) {
    if (state.changesList[payload.path]) {
      state.changesList[payload.path].timestamp = Date.now()
    } else {
      let changeType
      if (payload.type) {
        changeType = payload.type
      } else {
        changeType = 'Update'
      }
      let changeId
      if (payload.id) {
        changeId = payload.id
      } else {
        changeId = payload.path
      }
      const item = { id: changeId, type: changeType, timestamp: Date.now(), checked: true }
      // Need to use Vue.set here or Vue will not be able to track changes
      Vue.set(state.changesList, payload.path, item)
    }
  },
  toggleChangesListItem (state, path) {
    state.changesList[path].checked = !state.changesList[path].checked
  },
  clearChangesList (state, changes) {
    changes.forEach(item => {
      Vue.delete(state.changesList, item)
    })
  },
  clearAllChangesList (state, changes) {
    state.changesList = {}
  },
  setSaveStatus (state, status) {
    state.saveStatus = status
  }
}

const actions = {
  saveCheckedChanges (context) {
    const changes = []
    Object.keys(context.state.changesList).forEach((key) => {
      if (context.state.changesList[key].checked) {
        changes.push(key)
      }
    })
    context.dispatch('saveChanges', changes)
  },
  saveChanges (context, changes) {
    if (context.rootState.authState === 'logged in') {
      context.rootState.repository.commit(context, changes)
    } else {
      console.error("Can't save in demo")
    }
  },
  // TODO make this accept a list of changes to discard
  discardChanges (context, path) {
    context.rootState.repository.getFile(context, path)
  },
  discardAllChanges (context) {
    context.commit('clearAllChangesList')
    context.commit('sEdit/clearAllLoadedFile', '', { root: true })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
