import axios from 'axios'

const state = {
  projectId: '',
  CMS: '',
  demo: false
}

const getters = {

}

const mutations = {
  setProjectId (state, value) {
    state.projectId = value
  },
  setCMS (state, value) {
    let CMS = ''
    switch (String(value).toLowerCase) {
      case 'gitlab':
        CMS = 'gitlab'
        break
      case 'github':
        CMS = 'github'
        break
      default:
        CMS = ''
    }
    state.CMS = CMS
  },
  setDemo (state, value) {
    if (value) {
      state.demo = true
    }
  }
}

const actions = {
  loadConfig (context) {
    const jamdiv = document.querySelector('#jambalaya')
    if (jamdiv === null) {
      console.error('Jambalaya is missing the required configuration. (Error: config-1)')
      return
    }
    if (jamdiv.dataset.config) {
      let config
      axios.get(jamdiv.dataset.config)
        .then((res) => {
          config = res.data
          // Required config
          if (config.cms && config.projectId) {
            context.commit('setCMS', config.cms)
            context.commit('setProjectId', config.projectId)
            context.commit('setRepository', { cms: config.cms, projectId: config.projectId }, { root: true })
            context.commit('setTokenFromStogage', null, { root: true })
          } else {
            context.commit('setAuthState', 'need config', { root: true })
            // return so we do not set optional config demo
            return
          }

          // Optional configuration
          if (config.demo) {
            context.commit('setAuthState', 'demo', { root: true })
          }
        })
        .catch((err) => {
          context.commit('setAuthState', 'need config', { root: true })
          throw new Error('Unable to load config', err)
        })
    } else {
      if (jamdiv.dataset.cms && jamdiv.dataset.project) {
        context.commit('setProjectId', jamdiv.dataset.project)
        context.commit('setCMS', jamdiv.dataset.cms)
      } else {
        context.commit('setAuthState', 'need config', { root: true })
      }
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
