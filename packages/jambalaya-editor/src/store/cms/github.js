import axios from 'axios'

import AbstractCMS from './abstract'

export default class GitHub extends AbstractCMS {
  projectId = '';
  token = '';
  treeSha = '';

  constructor (projectId) {
    super()
    this.projectId = projectId
  }

  getUrl () {
    return `https://api.github.com/repos/${this.projectId}`
  }

  getToken () {
    return this.token || ''
  }

  getHeaders () {
    if (this.getToken().length > 0) {
      return {
        headers: {
          authorization: `token ${this.getToken()}`
        }
      }
    } else {
      return {}
    }
  }

  getTreeSha () {
    return this.treeSha || ''
  }

  commit (context, changes) {
    context.commit('sSave/setSaveStatus', 'loading', { root: true })
    this.loadSha(context, changes)
  }

  loadSha (context, changes, count) {
    if (count === undefined) {
      count = 0
    }
    if (count < changes.length) {
      axios.get(`${this.getUrl()}/contents/${changes[count]}`, this.getHeaders())
        .then((res) => {
          this.commitSingleFile(context, changes, count, changes[count], res.data.sha)
        })
        .catch(() => {
          context.commit('sSave/setSaveStatus', 'error', { root: true })
        })
    } else {
      context.commit('sSave/setSaveStatus', 'no changes', { root: true })
    }
  }

  commitSingleFile (context, changes, count, path, sha) {
    let commitData = {
      message: 'Jambalaya commit',
      content: btoa(context.rootState.sFile.loadedFiles[path]),
      sha: sha
    }
    axios.put(`${this.getUrl()}/contents/${path}`, commitData, this.getHeaders())
      .then((res) => {
        context.commit('sSave/clearChangesList', [path], { root: true })
        // Need this or you will be stuck in a loop
        this.loadSha(context, changes, count + 1)
      })
      .catch(() => {
        context.commit('sSave/setSaveStatus', 'error', { root: true })
      })
  }

  getFile (context, path) {
    axios.get(`${this.getUrl()}/contents/${path}`, this.getHeaders())
      .then((res) => {
        const content = atob(res.data.content)
        context.commit('sEdit/updateEditorDataRaw', content, { root: true })
        context.commit('sEdit/setEditorDataStatus', 'loaded', { root: true })
        context.commit('sFile/updateLoadedFiles', { path: path, value: content }, { root: true })
        context.commit('sSave/clearChangesList', [path], { root: true })
      })
      .catch(() => {
        context.commit('sEdit/setEditorDataStatus', 'error', { root: true })
      })
  }

  tree (context) {
    axios.get(`${this.getUrl()}/branches/master`, this.getHeaders())
      .then((branch) => {
        this.treeSha = branch.data.commit.commit.tree.sha
        this.loadTree(context)
      })
      .catch(() => {
        context.commit('sBrowse/setFileTreeStatus', 'error', { root: true })
      })
  }

  loadTree (context) {
    axios.get(`${this.getUrl()}/git/trees/${this.getTreeSha()}?recursive=true`, this.getHeaders())
      .then((tree) => {
        const files = []
        tree.data.tree.forEach((item) => {
          item.name = item.path.substring(item.path.lastIndexOf('/') + 1)
          if (item.type === 'tree') {
            const copy = Object.assign({}, item)
            copy.type = 'folder'
            files.push(copy)
          } else if (item.path.indexOf('.md') > 0) {
            const copy = Object.assign({}, item)
            copy.type = 'file'
            files.push(copy)
          }
        })
        context.commit('sBrowse/setFileTreeStatus', 'loaded', { root: true })
        context.commit('sBrowse/setFileTree', files, { root: true })
      })
      .catch(() => {
        context.commit('sBrowse/setFileTreeStatus', 'error', { root: true })
      })
  }

  auth (context, token) {
    axios.get(`${this.getUrl()}/branches/master`,
      {
        headers: {
          authorization: `token ${token}`
        }
      }
    )
      .then((res) => {
        this.token = token
        context.commit('setToken', token)
        context.commit('setAuthState', 'logged in')
      })
      .catch(() => {
        context.commit('setAuthState', 'error')
      })
  }
}
