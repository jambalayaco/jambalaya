import FM from 'gray-matter'
import Vue from 'vue'

const state = {
  currentlyEditing: '',
  editorDataStatus: 'needed',
  editorDataRaw: '',
  editorViewStates: {}
}

const getters = {
  editorDataFrontmatter: (state) => {
    let matter = { frontmatter: {}, content: '' }
    let gray = FM(state.editorDataRaw, {})
    try {
      matter.frontmatter = gray.data
    } catch (error) {
      throw new Error('Error setting frontmatter', error)
    }

    try {
      matter.content = gray.content
    } catch (error) {
      matter.content = ''
      throw new Error('Error setting frontmatter', error)
    }
    return matter
  },
  editorDataTabs: (state, getters) => {
    let tabs = []
    try {
      tabs = getters.editorDataFrontmatter.frontmatter['groups']
    } catch (error) {
      tabs = []
      throw new Error('Error setting tabs', error)
    }
    return tabs
  },
  editorDataActiveTab: (state, getters) => {
    let tabs = getters.editorDataTabs

    let activeTab = 'no tabs'
    // If there are groups, filter frontmatter to the active tab
    if (tabs && tabs.length > 0) {
      // Get the active tab and if it is not set, set it to the first item the groups array
      try {
        activeTab = state.editorViewStates[state.currentlyEditing].active
        if (activeTab.length === 0) {
          activeTab = tabs[0]
        }
      } catch (error) {
        activeTab = tabs[0]
      }
    }
    return activeTab
  },
  editorDataFields: (state, getters) => {
    // Filter frontmatter to only fields that are in the active group
    let fields = []
    let matter = getters.editorDataFrontmatter.frontmatter
    let activeTab = getters.editorDataActiveTab
    let fieldObj = {}
    Object.keys(matter).forEach((key) => {
      if (activeTab === 'no tabs' || matter[key].group === activeTab) {
        fieldObj = matter[key]
        fieldObj.id = key
        fields.push(fieldObj)
      }
    })
    return fields
  }
}

const mutations = {
  setEditorDataStatus (state, value) {
    state.editorDataStatus = value
  },
  updateEditorDataRaw (state, value) {
    state.editorDataRaw = value
  },
  setCurrentlyEditing (state, value) {
    if (value) {
      state.currentlyEditing = value
    } else {
      state.currentlyEditing = ''
    }
  },
  setEditorViewState (state, payload) {
    const tabState = payload ||
    {
      active: '',
      offset: 0
    }
    Vue.set(state.editorViewStates, state.currentlyEditing, tabState)
  }
}

const actions = {
  getEditorData (context) {
    let path = context.state.currentlyEditing
    // Check if we have the data in the changeList, if we do use it.  If not load it.
    if (path.length === 0) {
      context.commit('setEditorDataStatus', 'no file selected')
      context.commit('updateEditorDataRaw', '')
      return
    }
    const loadedData = context.rootState.sFile.loadedFiles[path]
    if (loadedData) {
      context.commit('updateEditorDataRaw', loadedData)
      context.commit('setEditorDataStatus', 'loaded')
    } else {
      context.commit('setEditorDataStatus', 'needed')
      context.rootState.repository.getFile(context, path)
      context.commit('setEditorViewState')
    }
  },
  updateDataRaw (context, value) {
    // Update editor data
    context.commit('updateEditorDataRaw', value)

    // Update loadedFiles
    context.commit('sFile/updateLoadedFiles', { value: value, path: context.state.currentlyEditing }, { root: true })

    // Update changesList
    context.commit('sSave/updateChangesList', { path: context.state.currentlyEditing }, { root: true })
  },
  updateDataFrontmatter (context, payload) {
    let temp = context.getters.editorDataFrontmatter.frontmatter
    try {
      temp[payload.id].value = payload.value
    } catch (error) {
      throw new Error('Trying to set a field that does not exsist')
    }
    context.dispatch(
      'updateDataRaw',
      FM.stringify(context.getters.editorDataFrontmatter.content, temp)
    )
  },
  updateDataContent (context, value) {
    context.dispatch(
      'updateDataRaw',
      FM.stringify(value, context.getters.editorDataFrontmatter.frontmatter)
    )
  },
  newContent (context, path) {
    context.commit('sFile/updateLoadedFiles', { value: ' ', path }, { root: true })
    context.commit('setCurrentlyEditing', path)
    context.commit('sSave/updateChangesList', { type: 'New', path }, { root: true })
    context.commit('sBrowse/addFileToTree', path, { root: true })
  }
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
