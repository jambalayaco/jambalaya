const plugin = require('tailwindcss/plugin')

module.exports = {
  important: '#jambalaya',
  purge: [
    './src/**/*.html',
    './src/**/*.vue'
  ],
  theme: {
    colors: {
      primary: '#ED1E79',
      dev: 'rgba(0, 255, 255, .5)',
      gray: {
        '100': 'rgba(242, 242, 242, 1)',
        '200': 'rgba(222, 222, 222, 1)',
        '300': 'rgba(204, 204, 204, 1)',
        '400': 'rgba(128, 128, 128, 1)',
        '500': 'rgba(102, 102, 102, 1)',
        '600': 'rgba(64, 64, 64, 1)',
        '700': 'rgba(36, 36, 36, 1)',
        '800': 'rgba(26, 26, 26, 1)',
        '900': 'rgba(13, 13, 13, 1)'
      }
    },
    extend: {
      spacing: {
        '.25x': '4px',
        '.5x': '8px',
        '1x': '16px',
        '1.5x': '24px',
        '2x': '32px',
        '2.5x': '40px',
        '3x': '48px',
        '4x': '64px',
        '8x': '128px',
        '10x': '160px',
        '16x': '256px',
        '20x': '320px',
        '32x': '512px',
        '40x': '640px'
      }
    }
  },
  variants: {
    textColor: ['hover, group-hover']
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      const newUtilities = {
        '.flex-center': {
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }
      }

      addUtilities(newUtilities)
    })
  ]
}
