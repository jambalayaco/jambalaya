import Vue from 'vue'
import Vuex from 'vuex'

import CMS from './cms'
import sBrowse from './modules/browser'
import sConfig from './modules/config'
import sEdit from './modules/edit'
import sFile from './modules/files'
import sSave from './modules/save'
import sView from './modules/views'

import { findBestMatch } from '../helpers/bestMatch'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    sBrowse,
    sConfig,
    sEdit,
    sFile,
    sSave,
    sView
  },
  state: {
    repository: new CMS(''),
    token: '',
    authState: ''
  },
  getters: {},
  mutations: {
    setToken (state, token) {
      localStorage.setItem('jamtoken', token)
      state.repository.token = token
      state.token = token
    },
    setTokenFromStogage (state) {
      const tkn = localStorage.getItem('jamtoken')
      if (tkn) {
        localStorage.setItem('jamtoken', tkn)
        state.repository.token = tkn
        state.token = tkn
        state.authState = 'logged in'
      }
    },
    setRepository (state, payload) {
      state.repository = new CMS(payload.cms, payload.projectId)
    },
    setAuthState (state, status) {
      state.authState = status
    }
  },
  actions: {
    init (context) {
      context.dispatch('sConfig/loadConfig', { root: true })
      const tkn = localStorage.getItem('jamtoken')
      if (tkn) {
        context.commit('setToken', tkn)
      } else {
        context.commit('setAuthState', 'logged out')
      }
      window.addEventListener('click', (event) => context.dispatch({
        type: 'onPageClick',
        event
      }))
    },
    logIn (context, token) {
      context.commit('setAuthState', 'loading')

      // TODO: should call a generic class?
      context.state.repository.auth(context, token)
      // gitLab(context, token);
    },
    logOut (context) {
      localStorage.removeItem('jamtoken')
      context.commit('setToken', '')
      context.commit('setAuthState', 'logged out')
    },
    onEditPage (context) {
      let possible = []
      let best = {}
      let page = ''
      try {
        var nodes = document.querySelectorAll('[data-jambalaya]')
        nodes.forEach(item => {
          if (String(item.dataset.jambalaya).includes('index.md')) {
            page = item.dataset.jambalaya
          }
          possible.push(item.dataset.jambalaya)
        })
        if (page.length < 1) {
          best = findBestMatch(window.location.href, possible)
          page = best.bestMatch.target
        }
      } catch (error) {
        console.error(error)
      }
      context.dispatch('sFile/openFile', page, { root: true })
    },
    onPageClick (context, click) {
      if (click.event.target.getAttribute('data-jambalaya') !== null) {
        context.dispatch('sFile/openFile', event.target.getAttribute('data-jambalaya'), { root: true })
      }
    }
  }
})
