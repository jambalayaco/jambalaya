const state = {
  currentlyDragging: { name: '', path: '' },
  fileTreeStatus: 'needed',
  fileTree: [],
  selectedFolder: ''
}

const getters = {
  fileTreeCount: (state) => state.fileTree.length
}

const mutations = {
  setFileTreeStatus (state, status) {
    state.fileTreeStatus = status
  },
  setFileTree (state, data) {
    state.fileTree = data
  },
  addFileToTree (state, path) {
    let name = path.substring(path.lastIndexOf('/') + 1)
    state.fileTree.push({
      id: 'new',
      mode: 'new',
      name: name,
      path: path,
      type: 'blob'
    })
  },
  setSelectedFolder (state, id) {
    state.selectedFolder = id
  },
  setDraggedFile (state, payload) {
    state.currentlyDragging = { name: payload.name, path: payload.path }
  }
}

const actions = {
  getFileTree (context) {
    context.commit('setFileTreeStatus', 'loading')
    context.rootState.repository.tree(context, context.rootState.token)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
