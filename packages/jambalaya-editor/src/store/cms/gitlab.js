import axios from 'axios'

import AbstractCMS from './abstract'

export default class GitLab extends AbstractCMS {
  projectId = '';
  token = '';

  constructor (projectId) {
    super()
    this.projectId = projectId
  }

  getUrl () {
    return `https://gitlab.com/api/v4/projects/${this.projectId}/repository`
  }

  getToken () {
    return this.token || ''
  }

  commit (context, changes) {
    context.commit('sSave/setSaveStatus', 'loading', { root: true })
    const commitData = {
      branch: 'master',
      commit_message: 'Jambalaya commit',
      actions: []
    }
    changes.forEach((item) => {
      let action
      switch (context.state.changesList[item].type) {
        case 'Update':
          action = 'update'
          break
        case 'New':
          action = 'create'
          break
        case 'Delete':
          action = 'delete'
          break
        case 'Move':
          // TODO need previous path as well for this to work
          action = 'move'
          break
        default:
          throw new Error('Invalid commit action')
      }
      commitData.actions.push(
        {
          action,
          file_path: item,
          content: context.rootState.sFile.loadedFiles[item]
        }
      )
    })
    axios.defaults.headers.common['PRIVATE-TOKEN'] = this.token
    axios.post(`${this.getUrl()}/commits`, commitData)
      .then(() => {
        context.commit('sSave/clearChangesList', changes, { root: true })
        context.commit('sSave/setSaveStatus', 'no changes', { root: true })
      })
      .catch((error) => {
        console.error(error)
      })
  }

  getFile (context, path) {
    const uri = encodeURIComponent(path)
    const url = `${this.getUrl()}/files/${uri}/raw?ref=master`
    axios.defaults.headers.common['PRIVATE-TOKEN'] = this.token
    axios.get(url)
      .then((res) => {
        context.commit('sEdit/updateEditorDataRaw', res.data, { root: true })
        context.commit('sEdit/setEditorDataStatus', 'loaded', { root: true })
        context.commit('sFile/updateLoadedFiles', { path: path, value: res.data }, { root: true })
        context.commit('sSave/clearChangesList', [path], { root: true })
      })
      .catch(() => {
        context.commit('sEdit/setEditorDataStatus', 'error', { root: true })
      })
  }

  tree (context) {
    axios.defaults.headers.common['PRIVATE-TOKEN'] = this.token
    // TODO Make this get additional pages and only content folders
    axios.get(`${this.getUrl()}/tree?recursive=1&per_page=1000`)
      .then((res) => {
        const files = []
        res.data.forEach((item) => {
          if (item.type === 'tree') {
            const copy = Object.assign({}, item)
            copy.type = 'folder'
            files.push(copy)
          } else if (item.name.indexOf('.md') > 0) {
            const copy = Object.assign({}, item)
            copy.type = 'file'
            files.push(copy)
          }
        })
        context.commit('sBrowse/setFileTreeStatus', 'loaded', { root: true })
        context.commit('sBrowse/setFileTree', files, { root: true })
      })
      .catch(() => {
        context.commit('sBrowse/setFileTreeStatus', 'error', { root: true })
      })
  }

  auth (context, token) {
    axios.defaults.headers.common['PRIVATE-TOKEN'] = token
    axios.get('https://gitlab.com/api/v4/user')
      .then((res) => {
        this.token = token
        context.commit('setToken', token)
        context.commit('setAuthState', 'logged in')
      })
      .catch(() => {
        context.commit('setAuthState', 'error')
      })
  }
}
