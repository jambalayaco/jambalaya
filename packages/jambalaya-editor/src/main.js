import Vue from 'vue'
import App from './App.vue'
import './styles/tailwind.css'

Vue.config.productionTip = false

new Vue({ // eslint-disable-line no-new
  el: '#jambalayaEditor',
  render: h => h(App)
})
