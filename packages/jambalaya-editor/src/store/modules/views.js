const state = {
  views: [
    { id: 'publish', icon: 'disk' },
    { id: 'edit', icon: 'edit' },
    { id: 'add', icon: 'add' },
    { id: 'browse', icon: 'tree' }
  ],
  activeView: '',
  backToView: ''
}

const getters = {
}

const mutations = {
  setActiveView (state, value) {
    if (value) {
      state.activeView = value
    } else {
      state.activeView = ''
    }
  },
  setBackToView (state, value) {
    if (value) {
      state.backToView = value
    } else {
      state.backToView = ''
    }
  }
}

const actions = {

}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
