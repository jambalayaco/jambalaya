import Vue from 'vue'
import { indexOfObjectWithKeyValue } from '../../helpers/indexOfObjectWithKeyValue'

const state = {
  loadedFiles: {},
  fileTabs: [
    { id: 'index.md', title: 'index.md' }
  ]
}

const getters = {}

const mutations = {
  updateLoadedFiles (state, payload) {
    Vue.set(state.loadedFiles, payload.path, payload.value)

    // Dispatch change event for SSG
    const event = new CustomEvent('jambalaya-event', { detail: state.loadedFiles })
    document.dispatchEvent(event)
  },
  clearAllLoadedFile () {
    state.loadedFiles = {}
  },
  setFileTabs (state, tabs) {
    state.fileTabs = tabs
  },
  addFileTab (state, path) {
    let title = path.substring(path.lastIndexOf('/') + 1)
    if (state.fileTabs.length < 4) {
      state.fileTabs.push({ id: path, title: title })
    } else {
      state.fileTabs.splice(3, 0, { id: path, title: title })
    }
  },
  removeFileTab (state, index) {
    state.fileTabs.splice(index, 1)
  },
  moveFileTab (state, payload) {
    let tab = state.fileTabs[payload.current]
    state.fileTabs.splice(payload.current, 1)
    state.fileTabs.splice(payload.new, 0, tab)
  }
}

const actions = {
  openFile (context, path) {
    let indexInTabs = indexOfObjectWithKeyValue(context.state.fileTabs, 'id', path)
    // If the tab does not exsist create it
    if (indexInTabs === -1) {
      context.commit('addFileTab', path)
    } else if (indexInTabs > 3) {
      context.commit('moveFileTab', { current: indexInTabs, new: 3 })
    }
    context.commit('sEdit/setCurrentlyEditing', path, { root: true })
    context.commit('sView/setActiveView', 'edit', { root: true })
  },
  closeFile (context, path) {
    let indexToRemove = -1
    state.fileTabs.forEach((elm, index) => {
      if (elm.id === path) {
        indexToRemove = index
      }
    })
    if (indexToRemove !== -1) {
      // If the tab was active, open next tab or previous tab if they exsist
      if (context.state.fileTabs[indexToRemove].id === context.rootState.sEdit.currentlyEditing) {
        if (context.state.fileTabs[indexToRemove + 1] !== undefined) {
          context.dispatch('openFile', context.state.fileTabs[indexToRemove + 1].id)
        } else if (context.state.fileTabs[indexToRemove - 1] !== undefined) {
          context.dispatch('openFile', context.state.fileTabs[indexToRemove - 1].id)
        } else {
          context.commit('sEdit/setCurrentlyEditing', '', { root: true })
        }
      }
      context.commit('removeFileTab', indexToRemove)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
